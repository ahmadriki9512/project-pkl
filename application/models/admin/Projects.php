<?php
class Projects extends CI_Model {
    
    function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    
   //Menampilan data

    public function display_record(){
        $query = $this->db->query("select * from project");
        $data = $query->result();

        return $data;
    }

    public function tambah_data($data){
        $query = $this->db->query("INSERT INTO project(nama,start,deadline,detail,progress,foto) VALUES ('".$data['nama']."','".$data['start']."','".$data['deadline']."','".$data['detail']."','".$data['progress']."','".$data['foto']."')");

        $this->db->set($query);
    }

    public function display_edit($data){
        $query = $this->db->query( "select * from project WHERE kode = '".$data."'");
        $row = $query->row();

        $edit['kode'] = $row->kode;
        $edit['nama'] = $row->nama;
        $edit['start'] = $row->start;
        $edit['deadline'] = $row->deadline;
        $edit['detail'] = $row->detail;
        $edit['progress'] = $row->progress;
        $edit['foto'] = $row->foto;

        return $edit;
    }

    public function delete($data){
        $query = $this->db->query( "DELETE  from project WHERE kode = '".$data."'");
        $this->db->set($query);
    }

    public function submit_edit($data){
        $query = $this->db->query( "UPDATE project SET nama = '".$data['nama']."', start = '".$data['start']."',deadline = '".$data['deadline']."',detail = '".$data['detail']."', progress = '".$data['progress']."', foto = '".$data['foto']."' WHERE kode ='".$data['kode']."'");

        $this->db->set($query);
    }
}
?>