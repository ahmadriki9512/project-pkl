-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.36-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for pkl
CREATE DATABASE IF NOT EXISTS `pkl` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pkl`;

-- Dumping structure for table pkl.client
CREATE TABLE IF NOT EXISTS `client` (
  `no` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_domain` varchar(50) DEFAULT NULL,
  `status` enum('AKTIF','TIDAK') DEFAULT NULL,
  `tgl_buat` date DEFAULT NULL,
  `tgl_exp` date DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `alamat_email` varchar(50) DEFAULT NULL,
  `no_telp` varchar(50) DEFAULT NULL,
  `paket` varchar(50) DEFAULT NULL,
  `tagihan` int(11) DEFAULT NULL,
  `tmpt_domain` varchar(50) DEFAULT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table pkl.client: ~3 rows (approximately)
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` (`no`, `nama_domain`, `status`, `tgl_buat`, `tgl_exp`, `penanggung_jawab`, `alamat_email`, `no_telp`, `paket`, `tagihan`, `tmpt_domain`, `keterangan`) VALUES
	(1, 'http://sinarmassejahtera.com', 'AKTIF', '2009-01-14', '2018-01-14', 'Bpk Lim', 'Jakarta', '081315527676', 'Platinum', 1000000, 'Resellerclub', ''),
	(4, 'RIKI', 'AKTIF', '1998-12-12', '2000-12-12', 'BOPAK', 'a.darmawan92@yahoo.com', '12', 'platinum@gmail.com', 1000, 'javk', 'BISA');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;

-- Dumping structure for table pkl.detail_project
CREATE TABLE IF NOT EXISTS `detail_project` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` int(11) DEFAULT NULL,
  `progress_harian` int(11) DEFAULT NULL,
  `deskripsi_fitur` varchar(500) DEFAULT NULL,
  `tanggal_post` date DEFAULT NULL,
  `jam_post` time DEFAULT NULL,
  PRIMARY KEY (`detail_id`),
  KEY `FK_detail_project_project` (`kode`),
  CONSTRAINT `FK_detail_project_project` FOREIGN KEY (`kode`) REFERENCES `project` (`kode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pkl.detail_project: ~1 rows (approximately)
/*!40000 ALTER TABLE `detail_project` DISABLE KEYS */;
INSERT INTO `detail_project` (`detail_id`, `kode`, `progress_harian`, `deskripsi_fitur`, `tanggal_post`, `jam_post`) VALUES
	(1, 16, 50, 'MENIKAH', '2020-05-27', '19:33:59');
/*!40000 ALTER TABLE `detail_project` ENABLE KEYS */;

-- Dumping structure for table pkl.project
CREATE TABLE IF NOT EXISTS `project` (
  `kode` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `detail` varchar(500) DEFAULT NULL,
  `progress` int(11) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table pkl.project: ~1 rows (approximately)
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`kode`, `nama`, `start`, `deadline`, `detail`, `progress`, `foto`) VALUES
	(10, 'ROYAN RUHYAN', '2009-12-10', '2020-10-10', 'AKU LAH YANG TETAP MEMELUKMU ERAT', 0, ''),
	(16, 'Sirah Nabi', '2009-12-12', '2020-12-12', 'AKU ADALAH GAMBANG', 0, 'ilove.jpg');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;

-- Dumping structure for table pkl.user
CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `level` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pkl.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`username`, `password`, `level`) VALUES
	('admin', 'admin', 1),
	('riki', 'riki', 2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
