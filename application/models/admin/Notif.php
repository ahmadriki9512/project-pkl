<?php
class Notif extends CI_Model {
    
    function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    
   //Menampilan data

    public function display_record(){
        $query = $this->db->query("select * from notif");
        $data = $query->result();

        return $data;
    }

    public function tambah_data($data,$tgl){
        $query = $this->db->query("INSERT IGNORE INTO notif (kode_client,tgl_notif,jumlah) SELECT ".$data.",'".$tgl."',COUNT(*)+1 FROM notif");

        $this->db->set($query);
    }

    public function delete($data){
        $query = $this->db->query( "DELETE  from notif WHERE kode_client = '".$data."'");
        $this->db->set($query);
    }
}
?>